require("awful")
vicious = require("vicious")

-- XXX handle muting (graph to full red?)

local dummy = widget({ type = "textbox"})   -- XXX: hack! avoid by wrapping vicious.widgets.volume?
local volumegraph = awful.widget.progressbar()
volumegraph:set_width(10)
volumegraph:set_height(15)
volumegraph:set_vertical(true)
volumegraph:set_max_value(100)
volumegraph:set_background_color("#000000")
volumegraph:set_gradient_colors({"#11dd00", "#dddd00"})

local function update(widget, args)
   volumegraph:set_value(args[1])
end
vicious.register(dummy, vicious.widgets.volume, update, 2, "Master")

return volumegraph.widget
