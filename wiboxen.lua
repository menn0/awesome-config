require("awful")

local wiboxen = {}

function wiboxen:addscreen(screen)
    self[screen] = awful.wibox({position="bottom", screen=screen})
    return self[screen]
end

return wiboxen
