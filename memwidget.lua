require("awful")
vicious = require("vicious")

local memwidget = widget({ type = "textbox" })
vicious.cache(vicious.widgets.mem)
vicious.register(memwidget, vicious.widgets.mem, " $4MB free ", 9)

return memwidget
