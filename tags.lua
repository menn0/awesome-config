require("layouts")

tag_names = {}

local function define_tag(t)
  -- with some fonts/resolutions tags look much nicer with some space
  -- around the numbers
  local n
  n = t
  if config.pad_tag_names then
    n = ' ' .. n .. ' '
  end
  table.insert(tag_names, n)
end

-- Define tags for all number keys
for t = 1, 9 do
   define_tag(t)
end
define_tag(0)

tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tag_names, s, layouts[1])
end
