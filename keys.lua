require("awful")
require("tags")

modkey = "Mod4"

globalkeys = awful.util.table.join(
    awful.key({ modkey, }, "Left",   awful.tag.viewprev),
    awful.key({ modkey, }, "Right",  awful.tag.viewnext),
    awful.key({ modkey, }, "h", awful.tag.viewprev),
    awful.key({ modkey, }, "l", awful.tag.viewnext),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
         end),

    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),
    awful.key({ modkey, }, "z", awful.tag.history.restore),

    awful.key({ modkey,           }, "Return", function () awful.util.spawn("binmenu") end),
    awful.key({ modkey,           }, "g", function () awful.util.spawn(config.browser) end),
    awful.key({},                    "Print", function () awful.util.spawn('gnome-screenshot --interactive') end),
    awful.key({ "Control"         }, "Print", function () awful.util.spawn('gnome-screenshot --area --interactive') end),
    awful.key({ "Mod1",           }, "Print", function () awful.util.spawn('gnome-screenshot --window') end),

    -- Switch US and GB keyboard layouts
    awful.key({ modkey,           }, "F7", function () awful.util.spawn("setxkbmap us -option ctrl:nocaps") end),
    awful.key({ modkey,           }, "F8", function () awful.util.spawn("setxkbmap gb -option ctrl:nocaps") end),

    awful.key({ modkey,           }, "F9", function () awful.util.spawn("quodlibet --previous") end),
    awful.key({ modkey,           }, "F10", function () awful.util.spawn("quodlibet --next") end),
    awful.key({ modkey,           }, "F11", function () awful.util.spawn("quodlibet --play-pause") end),
    awful.key({ modkey,           }, "F12", function () awful.util.spawn("quodlibet --pause") end),

    awful.key({ modkey, "Control" }, "r", awesome.restart),

    -- Change master window size (think < >)
    awful.key({ modkey,           }, ".",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, ",",     function () awful.tag.incmwfact(-0.05)    end),

    -- Change number of columns
    awful.key({ modkey, }, "equal",     function () awful.tag.incncol(1)         end),
    awful.key({ modkey, }, "minus",     function () awful.tag.incncol(-1)         end),

    -- Change number of clients in master area
    awful.key({ modkey, "Control"   }, "equal",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Control"   }, "minus",     function () awful.tag.incnmaster(-1)      end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey,           }, "Escape", function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",      function (c) c.minimized = not c.minimized    end),
    awful.key({ modkey }, "BackSpace", function (c) c.minimized = not c.minimized end),

    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end),
    awful.key({ modkey, "Shift" }, "t",
              function (c)
                 if c.titlebar then awful.titlebar.remove(c)
                 else awful.titlebar.add(c, { modkey = modkey }) end
              end
           )
)

-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 0.
for i, _ in ipairs(tag_names) do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

require_by_host("keys")    -- Per-host key customisations
root.keys(globalkeys)      -- Set keys
