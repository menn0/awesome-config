theme = {}
theme.confdir       = awful.util.getdir("config")
theme.icondir      = theme.confdir .. "/icons/"
theme.font          = config.font

-- Colors
theme.bg_normal     = "#222222"
theme.bg_focus      = "#666666"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.border_width  = "1"
theme.border_normal = "#222222"
theme.border_focus  = "yellow"
theme.border_marked = "#91231c"

-- Taglist squares
theme.taglist_squares_sel   = theme.confdir .. "/icons/taglist/sel.png"
theme.taglist_squares_unsel = theme.icondir .. "taglist/unsel.png"

-- Misc
theme.awesome_icon           = theme.icondir .. "awesome.png"
theme.tasklist_floating_icon = theme.icondir .. "tasklist/floatingw.png"

-- Menu
theme.menu_height = "15"
theme.menu_width  = "100"
theme.menu_submenu_icon      = theme.icondir .. "submenu.png"

-- Layouts
theme.layout_tile       = theme.icondir .. "layouts/tile.png"
theme.layout_tileleft   = theme.icondir .. "layouts/tileleft.png"
theme.layout_tilebottom = theme.icondir .. "layouts/tilebottom.png"
theme.layout_tiletop    = theme.icondir .. "layouts/tiletop.png"
theme.layout_fairv      = theme.icondir .. "layouts/fairv.png"
theme.layout_fairh      = theme.icondir .. "layouts/fairh.png"
theme.layout_spiral     = theme.icondir .. "layouts/spiral.png"
theme.layout_dwindle    = theme.icondir .. "layouts/dwindle.png"
theme.layout_max        = theme.icondir .. "layouts/max.png"
theme.layout_fullscreen = theme.icondir .. "layouts/fullscreen.png"
theme.layout_magnifier  = theme.icondir .. "layouts/magnifier.png"
theme.layout_floating   = theme.icondir .. "layouts/floating.png"

-- Widget icons
theme.widget_cpu    = theme.icondir .. "cpu.png"
theme.widget_bat    = theme.icondir .. "bat.png"
theme.widget_mem    = theme.icondir .. "mem.png"
theme.widget_fs     = theme.icondir .. "disk.png"
theme.widget_net    = theme.icondir .. "down.png"
theme.widget_netup  = theme.icondir .. "up.png"
theme.widget_wifi   = theme.icondir .. "wifi.png"
theme.widget_mail   = theme.icondir .. "mail.png"
theme.widget_vol    = theme.icondir .. "vol.png"
theme.widget_org    = theme.icondir .. "cal.png"
theme.widget_date   = theme.icondir .. "time.png"
theme.widget_crypto = theme.icondir .. "crypto.png"
theme.widget_sep    = theme.icondir .. "separator.png"

-- Titlebar
theme.titlebar_close_button_focus               = theme.icondir .. "titlebar/close_focus.png"
theme.titlebar_close_button_normal              = theme.icondir .. "titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active        = theme.icondir .. "titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = theme.icondir .. "titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = theme.icondir .. "titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = theme.icondir .. "titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active       = theme.icondir .. "titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = theme.icondir .. "titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = theme.icondir .. "titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = theme.icondir .. "titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active     = theme.icondir .. "titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = theme.icondir .. "titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = theme.icondir .. "titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = theme.icondir .. "titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active    = theme.icondir .. "titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = theme.icondir .. "titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.icondir .. "titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.icondir .. "titlebar/maximized_normal_inactive.png"


if (config.wallpaper) then
   theme.wallpaper_cmd = { "awsetbg " .. config.wallpaper }
else
   theme.wallpaper_cmd = { "true" }
end

return theme
