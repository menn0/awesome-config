require("awful")
vicious = require("vicious")

local cpugraph  = awful.widget.graph()
cpugraph:set_width(40):set_height(15)
cpugraph:set_background_color("#000000")
cpugraph:set_gradient_colors({"#005500", "#00ff00"})
vicious.register(cpugraph, vicious.widgets.cpu, "$1", 3)

local widget = cpugraph.widget

widget:buttons(awful.util.table.join(
    awful.button({}, 1, function () awful.util.spawn("gnome-system-monitor") end)
))

return widget
