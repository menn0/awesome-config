require("mouse")

function focus_screen(screen)
   awful.screen.focus(screen)
   -- Move mouse cursor to avoid gnome panel tooltip
   local m = mouse.coords()
   mouse.coords({ x = m.x, y = m.y + 32 })
end

globalkeys = awful.util.table.join(
    globalkeys,

    awful.key({ modkey,         }, "e", function () awful.util.spawn("/home/menno/bin/e") end),
    awful.key({ modkey,         }, "i", function () awful.util.spawn(config.terminal) end),
    awful.key({ modkey, "Shift" }, "i", function () awful.util.spawn("ipython-qtconsole") end),

    awful.key({ modkey,         }, "[", function () focus_screen(2) end),
    awful.key({ modkey,         }, "]", function () focus_screen(1) end),
    awful.key({ modkey, "Control" }, "h", function () focus_screen(2) end),
    awful.key({ modkey, "Control" }, "l", function () focus_screen(1) end),

    awful.key({}, "XF86Back", awful.tag.viewprev),
    awful.key({}, "XF86Forward", awful.tag.viewnext),
    awful.key({}, "Scroll_Lock", function () awful.util.spawn('gnome-screensaver-command --lock') end),

    awful.key({ modkey, "Control" }, "q", function() awful.util.spawn('gnome-session-quit') end)
)

clientkeys = awful.util.table.join(
    clientkeys,

    awful.key({ modkey, "Shift" }, "[", function () awful.client.movetoscreen(c, 2) end),
    awful.key({ modkey, "Shift" }, "]", function () awful.client.movetoscreen(c, 1) end),
    awful.key({ modkey, "Control", "Shift" }, "h", function () awful.client.movetoscreen(c, 2) end),
    awful.key({ modkey, "Control", "Shift" }, "l", function () awful.client.movetoscreen(c, 1) end)
)
