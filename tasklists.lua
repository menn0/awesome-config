require("awful")

local tasklists = {}

tasklists.buttons = awful.util.table.join(
    awful.button({}, 1, function (c)
                            if not c:isvisible() then
                               awful.tag.viewonly(c:tags()[1])
                            end
                            -- This will also un-minimize
                            -- the client, if needed
                            client.focus = c
                            c:raise()
                        end),
    awful.button({}, 2, function ()
                             if client_menu then
                                 client_menu:hide()
                                 client_menu = nil
                             else
                                 client_menu = awful.menu.clients({ width=250 })
                             end
                         end),
    awful.button({}, 3, function (c)
                            c.minimized = true
                        end),
    awful.button({}, 4, function ()
                            awful.client.focus.byidx(1)
                            if client.focus then
                               client.focus:raise()
                            end
                        end),
    awful.button({}, 5, function ()
                            awful.client.focus.byidx(-1)
                            if client.focus then
                               client.focus:raise()
                            end
                        end)
    )

function tasklists:addscreen(screen)
    self[screen] = awful.widget.tasklist(
       function(c)
          return awful.widget.tasklist.label.currenttags(c, screen)
       end,
       self.buttons
    )
    return self[screen]
end

return tasklists
