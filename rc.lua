require("utils")

config = require_by_host("config")

require("naughty")    -- Notification library
require("beautiful")  -- Theme handling library
beautiful.init(awful.util.getdir('config') .. "/theme.lua")
require("awful.autofocus")

require("layouts")
require("tags")
require("keys")
require("buttons")
require_by_host("panel")
require("rules")
require("signals")
