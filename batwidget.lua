require("awful")
vicious = require("vicious")

local batwidget = widget({type = "textbox"})
vicious.register(batwidget, vicious.widgets.bat, "[$1$2% $3]", 5, "BAT0")

return batwidget
