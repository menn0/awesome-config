require("awful.rules")

require("keys")
require("buttons")

-- Rules
awful.rules.rules = {
    -- All clients will match this rule.
   {
      rule = {},
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     maximized_vertical   = false,
                     maximized_horizontal = false }
   },
   {
      rule = { name="Calendar" },
      properties = { floating = true, ontop = true },
   },
   {
      rule = { name="Cisco AnyConnect Secure Mobility Client" },
      properties = { floating = true, ontop = true },
   },
   {
      rule = { class="Gkrellm" },
      properties = { floating = true, ontop = true, sticky = true },
   },
}
