require('freedesktop.utils')
require('freedesktop.menu')

freedesktop.utils.terminal = config.terminal

menu_items = freedesktop.menu.new()
local awesomemenu = {
   { "Restart", awesome.restart,
     freedesktop.utils.lookup_icon({ icon = 'gtk-refresh' }) },
   { "Quit", awesome.quit,
     freedesktop.utils.lookup_icon({ icon = 'gtk-quit' }) }
}
table.insert(menu_items, { "Awesome", awesomemenu, beautiful.awesome_icon })

local mainmenu = awful.menu.new({items = menu_items, width = 200})

local launcher = awful.widget.launcher({image = image(beautiful.awesome_icon),
                                  command = ""})
launcher:buttons(awful.util.table.join(
  launcher:buttons(),
  awful.button({}, 1,
               function ()
                  mainmenu:toggle({keygrabber = true})
               end))
)
return launcher
