require("awful")

local function find_screen_left()
   local screen = mouse.screen
   if (screen == 2) or (screen == 3) then
      return screen - 1
   elseif screen == 4 then
      return 1
   end
   return screen
end

local function find_screen_right()
   local screen = mouse.screen
   if screen <= 2 then
      return screen + 1
   elseif screen == 4 then
      return 3
   end
   return screen
end

local function find_screen_up()
   local screen = mouse.screen
   if screen == 4 then
      return 2
   end
   return screen
end

local function find_screen_down()
   return 4
end

local function move_client_tag_and_follow(c, offset)
    local newidx = awful.tag.getidx(c:tags()[1]) + offset
    local newtag
    -- XXX don't hardcode 9 here
    if newidx < 1 then
       newidx = 9
    end
    if newidx > 9 then
       newidx = 1
    end
    newtag = screen[mouse.screen]:tags()[newidx]
    c:tags({newtag})
    awful.tag.viewonly(newtag)
end

local function move_client_tag_left(c)
   move_client_tag_and_follow(c, -1)
end

local function move_client_tag_right(c)
   move_client_tag_and_follow(c, 1)
end

local sound_card_id  = 0
local sound_channel = "Master"

local function volume_up()
   io.popen("amixer -q -c " .. sound_card_id .. " sset " .. sound_channel .. " 2%+"):read("*all")
end

local function volume_down()
   io.popen("amixer -q -c " .. sound_card_id .. " sset " .. sound_channel .. " 2%-"):read("*all")
end

local function volume_mute_toggle()
   io.popen("amixer -c " .. sound_card_id .. " sset " .. sound_channel .. " toggle"):read("*all")
end


globalkeys = awful.util.table.join(
   globalkeys,

   awful.key({ modkey,         }, "i", function () awful.util.spawn(config.terminal .. ' -e ssh scomslin') end),
   awful.key({ modkey, "Shift" }, "i", function () awful.util.spawn(config.terminal) end),

   awful.key({ modkey,         }, "e", function () awful.util.spawn('ssh scomslin e') end),
   awful.key({ modkey, "Shift" }, "e", function () awful.util.spawn("e") end),

   awful.key({ modkey, }, "Insert", function () awful.screen.focus(1) end),
   awful.key({ modkey, }, "Home", function () awful.screen.focus(2) end),
   awful.key({ modkey, }, "Prior", function () awful.screen.focus(3) end),
   awful.key({ modkey, }, "End", function () awful.screen.focus(4) end),

   awful.key({ modkey, "Control" }, "h",
             function () awful.screen.focus(find_screen_left()) end),
   awful.key({ modkey, "Control" }, "l",
             function () awful.screen.focus(find_screen_right()) end),
   awful.key({ modkey, "Control" }, "j",
             function () awful.screen.focus(find_screen_down()) end),
   awful.key({ modkey, "Control" }, "k",
             function () awful.screen.focus(find_screen_up()) end),

    awful.key({ modkey, "Shift" }, "h", move_client_tag_left),
    awful.key({ modkey, "Shift" }, "l", move_client_tag_right),

    awful.key({ modkey, "Shift" }, "g", function () awful.util.spawn('opera --newwindow') end),
    awful.key({ modkey, "Control" }, "g", function () awful.util.spawn('run-chrome') end),
    awful.key({ modkey, }, "o", function () awful.util.spawn('sshmenu -short') end),
    awful.key({ modkey, "Shift" }, "o", function () awful.util.spawn('sshmenu') end),
    awful.key({ }, "Scroll_Lock", function () awful.util.spawn('lock-screen') end),
    awful.key({}, "Pause", volume_mute_toggle),
    awful.key({modkey}, "Up", volume_up),
    awful.key({modkey}, "Down", volume_down),

    awful.key({ modkey, "Control" }, "q", awesome.quit)
)

clientkeys = awful.util.table.join(
   clientkeys,
   awful.key({ modkey, "Shift" }, "h", move_client_tag_left),
   awful.key({ modkey, "Shift" }, "l", move_client_tag_right),

   awful.key({ modkey, "Control", "Shift" }, "h", function () awful.client.movetoscreen(c, find_screen_left()) end),
   awful.key({ modkey, "Control", "Shift" }, "l", function () awful.client.movetoscreen(c, find_screen_right()) end),
   awful.key({ modkey, "Control", "Shift" }, "j", function () awful.client.movetoscreen(c, find_screen_down()) end),
   awful.key({ modkey, "Control", "Shift" }, "k", function () awful.client.movetoscreen(c, find_screen_up()) end),

   awful.key({ modkey, "Shift" }, "Insert", function () awful.client.movetoscreen(c, 1) end),
   awful.key({ modkey, "Shift" }, "Home", function () awful.client.movetoscreen(c, 2) end),
   awful.key({ modkey, "Shift" }, "Prior", function () awful.client.movetoscreen(c, 3) end),
   awful.key({ modkey, "Shift" }, "End", function () awful.client.movetoscreen(c, 4) end)
)
