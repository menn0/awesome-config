require("awful")
require("awful.util")

function require_by_host(prefix)
   hostname = awful.util.pread('hostname -s'):gsub('\n', '')
   return require(prefix .. "-" .. hostname)
end

function load_icon(path)
    local icon = widget({ type = "imagebox" })
    icon.image = image(path)
    return icon
end
