require("awful")
require("beautiful")
require("image")
require("utils")
vicious = require("vicious")

local clockwidget = awful.widget.textclock({align="right"}, "%a %b %d, %H:%M:%S %Z", 1)

local function get_CST_time()
   local filedescriptor = io.popen("TZ=America/Chicago date +'%H:%M %Z'")
   local value = filedescriptor:read()
   filedescriptor:close()
   return {value}
end
local clockwidget2 = widget({type = "textbox"})
vicious.register(clockwidget2, get_CST_time, "$1  |", 29)

-- BATS environment status
local function get_bats_status()
    local filedescriptor = io.popen('~/mybin/bats-status.py')
    local value = filedescriptor:read()
    filedescriptor:close()
    return {value}
end
local batswidget = widget({type = "textbox"})
vicious.register(batswidget, get_bats_status, "$1", 10)

local cpugraph = require("cpugraph")
local memgraph = require("memgraph")
local volumewidget = require("volumewidget")
local systray = widget({type = "systray"})
local taglists = require("taglists")
local tasklists = require("tasklists")
local layoutboxen = require("layoutboxen")
local wiboxen = require("wiboxen")

local graphmargins = { top=2, right=3 }
awful.widget.layout.margins[cpugraph] = graphmargins
awful.widget.layout.margins[memgraph] = graphmargins
awful.widget.layout.margins[volumewidget] = graphmargins
awful.widget.layout.margins[clockwidget] = { right=3 }
awful.widget.layout.margins[clockwidget2] = { right=3 }
awful.widget.layout.margins[systray] = { top=1, right=3 }

for s = 1, screen.count() do
   wibox = wiboxen:addscreen(s)
   wibox.widgets = {
      {
         taglists:addscreen(s),
         layout = awful.widget.layout.horizontal.leftright
      },
      layoutboxen:addscreen(s),
      s == 2 and batswidget,
      s == 1 and clockwidget,
      s == 1 and clockwidget2,
      s == 1 and systray,
      s == 1 and volumewidget,
      s == 1 and load_icon(beautiful.widget_vol),
      s == 1 and memgraph,
      s == 1 and load_icon(beautiful.widget_mem),
      s == 1 and cpugraph,
      s == 1 and load_icon(beautiful.widget_cpu),
      tasklists:addscreen(s),
      layout = awful.widget.layout.horizontal.rightleft
   }
end
