require("awful")
vicious = require("vicious")


local memgraph  = awful.widget.graph()
memgraph:set_width(40):set_height(15)
memgraph:set_background_color("#000000")
memgraph:set_gradient_colors({"#555500", "#ffff00"})
vicious.cache(vicious.widgets.mem)
vicious.register(memgraph, vicious.widgets.mem, "$1", 3)

local widget = memgraph.widget

widget:buttons(awful.util.table.join(
    awful.button({}, 1, function () awful.util.spawn("gnome-system-monitor") end)
))

return widget
