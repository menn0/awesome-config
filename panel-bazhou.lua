require("awful")

local taglists = require("taglists")
local tasklists = require("tasklists")
local layoutboxen = require("layoutboxen")
local wiboxen = require("wiboxen")

-- Just show layout and windows at the bottom of the 
-- screen. Gnome will handle other widgets on a panel.
for s = 1, screen.count() do
   wibox = wiboxen:addscreen(s)
   wibox.widgets = {
      {
           taglists:addscreen(s),
           layout = awful.widget.layout.horizontal.leftright
      },
      layoutboxen:addscreen(s),
      tasklists:addscreen(s),
      layout = awful.widget.layout.horizontal.rightleft
   }
end
