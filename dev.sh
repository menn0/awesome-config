#!/bin/bash

Xephyr -ac -screen 1000x700 :2 &> /dev/null &
DISPLAY=:2 PS1="(xephyr) $(pwd \l)\u@\h:\w>" /bin/bash
pkill Xephyr

