require("awful")
layouts = require("layouts")

local layoutboxen = {}

-- Create an imagebox widget which will contains an icon
-- indicating which layout we're using.
function layoutboxen:addscreen(screen)
    self[screen] = awful.widget.layoutbox(screen)
    self[screen]:buttons(awful.util.table.join(
        awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
        awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
        awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
        awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)
    ))
    return self[screen]
end

return layoutboxen
